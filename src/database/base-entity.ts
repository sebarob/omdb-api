import {CreateDateColumn, UpdateDateColumn, VersionColumn} from 'typeorm';
import {Exclude} from 'class-transformer';

export abstract class BaseEntity {

  @Exclude()
  @CreateDateColumn()
  __createdAt: Date;

  @Exclude()
  @UpdateDateColumn()
  __updatedAt: Date;

  @Exclude()
  @VersionColumn()
  __version: number;

}
