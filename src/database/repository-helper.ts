import {IPaginationParams} from '../decorators/pagination-params';
import {classToPlain} from 'class-transformer';

export interface IPaginatedData<T = any> {
  pageIndex: number;
  pageSize: number;
  total: number;
  rows: T[];
}

export async function paginate(params: IPaginationParams): Promise<IPaginatedData<any>> {
  const [rows, count] = await this.findAndCount({
    skip: params.pageIndex * params.pageSize,
    take: params.pageSize,
  });

  return {
    pageIndex: params.pageIndex,
    pageSize: params.pageSize,
    total: count,
    rows: rows.map(row => classToPlain(row)),
  };
}