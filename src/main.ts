import {AppModule} from './app.module';
import {NestFactory} from '@nestjs/core';
import {useContainer} from 'class-validator';
import * as helmet from 'helmet';
import {json, urlencoded} from 'body-parser';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';


function globalMiddleware(app) {
  app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal']);

  app.use(helmet());
  app.use(json());
  app.use(urlencoded({extended: false}));
}

function enableSwagger(app) {
  const options = new DocumentBuilder()
      .setTitle('OMDB - Client')
      .setDescription('Recruitment task')
      .setVersion('1.0')
      .setSchemes('http')
      .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/swagger', app, document);
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  useContainer(app.select(AppModule), {fallbackOnErrors: true});

  globalMiddleware(app);
  enableSwagger(app);

  await app.listen(3000);
}
bootstrap();
