import {Inject, Injectable, OnModuleInit} from '@nestjs/common';
import * as elasticSearch from 'elasticsearch';
import {Client, SearchParams} from 'elasticsearch';
import {IndexDocumentParams} from 'elasticsearch';
import {DeleteDocumentParams} from 'elasticsearch';
import {APP_CONFIG} from '../configuration/helpers';
import {AppConfig} from '../configuration/interfaces';

@Injectable()
export class ElasticsearchService implements OnModuleInit {

  private client: Client;

  constructor(
      @Inject(APP_CONFIG)
      private readonly config: AppConfig,
  ) {}

  async onModuleInit() {
    this.client = await new elasticSearch.Client({host: this.config.elasticSearch.host});
  }

  public async registerIndice(indiceName) {
    try {
      return await this.client.indices.get({index: indiceName});
    } catch (error) {
      return await this.client.indices.create({index: indiceName});
    }
  }

  public createDocument<T>(document: IndexDocumentParams<T>) {
    return this.client.index(document);
  }

  public updateDocument<T>(document: IndexDocumentParams<T>) {
    return this.client.index(document);
  }

  public deleteDocument(document: DeleteDocumentParams) {
    return this.client.delete(document);
  }

  public search(searchParams: SearchParams) {
    return this.client.search(searchParams);
  }

}