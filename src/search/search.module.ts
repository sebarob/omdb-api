import {Module} from '@nestjs/common';
import {ElasticsearchService} from './elasticsearch.service';
import {ConfigProvider} from '../configuration/helpers';

@Module({
  providers: [
      ConfigProvider,
      ElasticsearchService,
  ],
  exports: [ElasticsearchService],
})
export class SearchModule {

}