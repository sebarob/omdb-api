import {registerDecorator, ValidationArguments, ValidationOptions} from 'class-validator';

export function ThrowIf(expression, validationOptions?: ValidationOptions) {
  return function(object: object, propertyName: string) {
    registerDecorator({
      name: 'throwIf',
      target: object.constructor,
      propertyName,
      constraints: [expression],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const constraint: (ctx) => boolean = args.constraints[0];
          const shouldThrow = !constraint(args.object);
          return shouldThrow;
        },
      },
    });
  };
}