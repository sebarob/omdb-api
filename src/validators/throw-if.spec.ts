import {ThrowIf} from './throw-if';
import {validate, ValidationError} from 'class-validator';

class ThrowIfSpec {

  static value;

  @ThrowIf(() => ThrowIfSpec.value)
  test1 = 'string';

  constructor(value) {
    ThrowIfSpec.value = value;
  }

}

describe('ThrowIf decorator', () => {

  it('should return error given truthy expression', async () => {
    const result = await validate(new ThrowIfSpec(true));
    expect(result[0] instanceof ValidationError).toBe(true);
  });

  it('shouldn\'t return error given falsy expression', async () => {
    let result;
    result = await validate(new ThrowIfSpec(false));
    expect(result[0]).toBeFalsy();

    result = await validate(new ThrowIfSpec(0));
    expect(result[0]).toBeFalsy();

    result = await validate(new ThrowIfSpec(null));
    expect(result[0]).toBeFalsy();

    result = await validate(new ThrowIfSpec(''));
    expect(result[0]).toBeFalsy();
  });

});
