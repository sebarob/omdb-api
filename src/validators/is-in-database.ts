import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import {Connection, Repository} from 'typeorm';
import {Injectable} from '@nestjs/common';

@ValidatorConstraint({async: true})
@Injectable()
export class IsInDatabaseConstraint implements ValidatorConstraintInterface {

  constructor(
      private connection: Connection,
  ) {}

  validate(value: any, args: ValidationArguments) {
    const [entityType, property] = args.constraints;
    const entityRepository: Repository<any> = this.connection.getRepository(entityType);

    return entityRepository.findOne({
      where: {[property]: value},
    });
  }

}

export function IsInDatabase(entityType, property, validationOptions?: ValidationOptions) {
  return function(object: object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [entityType, property],
      validator: IsInDatabaseConstraint,
    });
  };
}