import {useContainer, validate, ValidationError} from 'class-validator';
import {IsInDatabase, IsInDatabaseConstraint} from './is-in-database';
import {Test} from '@nestjs/testing';
import {Connection, Repository} from 'typeorm';

const ENTITY = Symbol();

class IsInDatabaseSpec {

  @IsInDatabase(ENTITY, 'test')
  prop = 'test';

}

describe('IsInDatabase decorator', () => {
  let app;

  const mockedReposiotry = {
    findOne: async () => {
    },
  };
  const mockedConnection: Partial<Connection> = {
    getRepository: (token) => mockedReposiotry as Repository<any>,
  };

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [
        {
          provide: Connection,
          useValue: mockedConnection,
        },
        IsInDatabaseConstraint,
      ],
    }).compile();

    useContainer(app, {fallbackOnErrors: true});
  });

  it('should return error if entity not in database', async () => {
    jest.spyOn(mockedReposiotry, 'findOne').mockImplementation(() => undefined);

    const result = await validate(new IsInDatabaseSpec());
    expect(result[0] instanceof ValidationError).toBe(true);
  });

  it('shouldn\'t return error if entity not in database', async () => {
    jest.spyOn(mockedReposiotry, 'findOne').mockImplementation(() => true);

    const result = await validate(new IsInDatabaseSpec());
    expect(result[0]).toBeFalsy();
  });

});
