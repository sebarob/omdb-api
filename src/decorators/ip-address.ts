import {createParamDecorator} from '@nestjs/common';

export const IPAddress = createParamDecorator((data, req) => req.ip)