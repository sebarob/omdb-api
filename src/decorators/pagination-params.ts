import {createParamDecorator} from '@nestjs/common';

export const DEFAULT_PAGE_SIZE = 20;
export const DEFAULT_PAGE_INDEX = 0;

const getPageSize = (pageSize) => {
  const parsedPageSize = parseInt(pageSize, 10);
  if (!parsedPageSize) {
    return DEFAULT_PAGE_SIZE;
  }

  return parsedPageSize > 100 || parsedPageSize < 1 ? DEFAULT_PAGE_SIZE : parsedPageSize;
};

const getPageIndex = (pageIndex) => {
  const parsedPageIndex = parseInt(pageIndex, 10);
  if (!parsedPageIndex) {
    return 0;
  }

  return parsedPageIndex < 0 ? DEFAULT_PAGE_INDEX : parsedPageIndex;
};

export interface IPaginationParams {
  pageSize: number;
  pageIndex: number;
}

export const paginationParamsParser = (data, req): IPaginationParams => {
  const query = req.query;

  return {
    pageSize: getPageSize(query.pageSize),
    pageIndex: getPageIndex(query.pageIndex),
  };
}

export const PaginationParams = createParamDecorator(paginationParamsParser);
