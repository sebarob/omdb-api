import {DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE, IPaginationParams, paginationParamsParser} from './pagination-params';
import {isEqual} from 'lodash';

const defaultParams: IPaginationParams = {
  pageSize: DEFAULT_PAGE_SIZE,
  pageIndex: DEFAULT_PAGE_INDEX,
}

describe('PaginationParams decorator', () => {

  it('should return default values for empty params', () => {
    const result = paginationParamsParser(undefined, {query: {}});
    expect(isEqual(result, defaultParams)).toBeTruthy();
  });

  it('should parse strings to numbers', () => {
    const result = paginationParamsParser(undefined, {query: {pageIndex: '10', pageSize: '10'}});
    expect(isEqual(result, {pageIndex: 10, pageSize: 10})).toBeTruthy();
  });

  it('should return default pageIndex for negative number', () => {
    const result = paginationParamsParser(undefined, {query: {pageIndex: '-10'}});
    expect(result.pageIndex).toBe(0);
  });

  it('should return default pageSize for negative number', () => {
    const result = paginationParamsParser(undefined, {query: {pageSize: '-10'}});
    expect(result.pageSize).toBe(20);
  });

  it('should return default values for non parsable strings', () => {
    const result = paginationParamsParser(undefined, {query: {pageSize: '**', pageIndex: '**'}});
    expect(isEqual(result, defaultParams)).toBeTruthy();
  });

});
