import {ApiModelProperty} from '@nestjs/swagger';
import {IPaginatedData} from '../database/repository-helper';

export class PaginatedResDto implements Partial<IPaginatedData<any>> {

  @ApiModelProperty()
  pageSize: number;

  @ApiModelProperty()
  pageIndex: number;

  @ApiModelProperty()
  total: number;

}