import {IPaginationParams} from '../decorators/pagination-params';
import {IsNumberString} from 'class-validator';
import {ApiModelPropertyOptional} from '@nestjs/swagger';

export class PaginatedReqQueryDto implements IPaginationParams {

  @ApiModelPropertyOptional({description: 'Size of page. (0 < x < 100)'})
  @IsNumberString()
  pageSize: number;

  @ApiModelPropertyOptional({description: 'Index of page. (0 < x)'})
  @IsNumberString()
  pageIndex: number;
}