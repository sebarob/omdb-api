import {Module} from '@nestjs/common';
import {ConfigurationService} from './configuration/configuration.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {first, map} from 'rxjs/operators';
import {MoviesModule} from './movies/movies.module';
import {ConfigurationModule} from './configuration/configuration.module';
import {IsInDatabaseConstraint} from './validators/is-in-database';

@Module({
  imports: [
    ConfigurationModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigurationModule],
      useFactory: (configurationService: ConfigurationService) => {
        return configurationService.config$.pipe(
            map(config => config.database),
            first(),
        ).toPromise();
      },
      inject: [ConfigurationService],
    }),
    MoviesModule,
  ],
  controllers: [],
  providers: [
    IsInDatabaseConstraint,
  ],
})
export class AppModule {
}
