import {Injectable} from '@nestjs/common';
import {BehaviorSubject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {AppConfig} from './interfaces';
import {CONFIG_PROD} from './configs/prod';
import {CONFIG_DEV} from './configs/dev';

@Injectable()
export class ConfigurationService {

  private configSubject = new BehaviorSubject<AppConfig>(null);
  public config$ = this.configSubject.asObservable().pipe(filter(Boolean));

  constructor() {
    this.init();
  }

  init() {
    this.configSubject.next(this.getConfiguration());
  }

  getConfiguration() {
    switch (process.env.NODE_ENV) {
      case 'production':
        return CONFIG_PROD;

      default:
        return CONFIG_DEV;
    }
  }
}
