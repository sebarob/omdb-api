import {AppConfig} from '../interfaces';

export const CONFIG_PROD: AppConfig = {
  database: {
    type: 'mysql',
    host: 'omdb-mysql',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'omdb',
    entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
    synchronize: true,
  },
  movieApi: {
    url: 'http://www.omdbapi.com/',
    key: 'cc90dbf0',
  },
  elasticSearch: {
    host: 'omdb-elasticsearch:9200',
  },
  redis: {
    port: 6379,
    host: 'omdb-redis',
  },
};
