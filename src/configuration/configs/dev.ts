import {AppConfig} from '../interfaces';


export const CONFIG_DEV: AppConfig = {
  database: {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'omdb',
    entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
    synchronize: true,
    logging: true,
  },
  movieApi: {
    url: 'http://www.omdbapi.com/',
    key: 'cc90dbf0',
  },
  elasticSearch: {
    host: 'localhost:9200',
  },
  redis: {
    port: 6379,
    host: '127.0.0.1',
  },
};
