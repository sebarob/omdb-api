export interface IMovieApi {
  url: string;
  key: string;
}

export interface IDatabaseConfig {
  type: string;
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
  entities: string[];
  synchronize?: boolean;
  logging?: boolean;
}

export interface IRedisConfig {
  port: number;
  host: string;
}

export interface IElasticSearchConfig {
  host: string;
}

export interface AppConfig {
  database: IDatabaseConfig;
  movieApi: IMovieApi;
  redis: IRedisConfig;
  elasticSearch: IElasticSearchConfig;
}

export type KeyOfAppConfig = keyof AppConfig;
