import {first} from 'rxjs/operators';
import {ConfigurationService} from './configuration.service';

export const APP_CONFIG = 'Config';

export const ConfigProvider = {
  provide: APP_CONFIG,
  useFactory: (configService: ConfigurationService) => {
    return configService.config$.pipe(
        first(),
    ).toPromise();
  },
  inject: [ConfigurationService],
};