import {MovieEntity} from '../entities/movie.entity';
import {EntitySubscriberInterface, InsertEvent, RemoveEvent, UpdateEvent} from 'typeorm';
import {Injectable} from '@nestjs/common';
import {ElasticsearchService} from '../../../search/elasticsearch.service';
import {movieIndex} from '../../movies.module';
import {InjectedEventSubscriber} from '../../../decorators';


@Injectable()
@InjectedEventSubscriber()
export class MovieSubscriber implements EntitySubscriberInterface<MovieEntity> {

  constructor(private elasticSearchService: ElasticsearchService) {}

  listenTo() {
    return MovieEntity;
  }

  async afterInsert(event: InsertEvent<MovieEntity>) {
    const {__version, ...entity} = event.entity;
    await this.elasticSearchService.createDocument<Partial<MovieEntity>>({
      index: movieIndex,
      type: movieIndex,
      id: entity.id,
      body: entity,
    });
    console.log(`Movie inserted`);
  }

  async afterUpdate(event: UpdateEvent<MovieEntity>) {
    const {__version, ...entity} = Object.assign(event.databaseEntity, event.entity);
    await this.elasticSearchService.updateDocument<Partial<MovieEntity>>({
      index: movieIndex,
      type: movieIndex,
      id: entity.id,
      body: entity,
    });
    console.log(`Movie updated`);
  }

  async afterRemove(event: RemoveEvent<MovieEntity>) {
    await this.elasticSearchService.deleteDocument({
      index: movieIndex,
      type: movieIndex,
      id: event.databaseEntity.id,
    });
    console.log(`Movie deleted`);
  }
}