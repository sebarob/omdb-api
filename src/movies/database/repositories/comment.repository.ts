import {Repository, EntityRepository} from 'typeorm';
import {MovieCommentEntity} from '../entities/movie-comment.entity';

@EntityRepository(MovieCommentEntity)
export class MovieCommentRepository extends Repository<MovieCommentEntity> {


}