import {EntityRepository, In, Repository} from 'typeorm';
import {MovieEntity} from '../entities/movie.entity';
import {paginate as paginator} from '../../../database/repository-helper';

interface IPaginatedData<T> {
  pageIndex: number;
  pageSize: number;
  total: number;
  rows: T[];
}

@EntityRepository(MovieEntity)
export class MovieRepository extends Repository<MovieEntity> {

  public paginate: (p) => Promise<IPaginatedData<MovieEntity>> = paginator.bind(this);

  findByIds(ids: string[]) {
    return this.find({
      where: {
        id: In(ids),
      },
    });
  }
}