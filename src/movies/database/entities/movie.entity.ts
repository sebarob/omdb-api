import {Column, OneToMany, Entity, PrimaryGeneratedColumn, DeepPartial, Generated, Index} from 'typeorm';
import {TOMDBMovieType} from '../../services/movie/movies-loader/interfaces';
import {MovieRatingEntity} from './movie-rating.entity';
import {BaseEntity} from '../../../database/base-entity';
import {MovieCommentEntity} from './movie-comment.entity';
import {Type} from 'class-transformer';

@Entity()
export class MovieEntity extends BaseEntity {

  @Index()
  @Generated('uuid')
  @Column()
  id: string;

  @Column()
  title: string;

  @Column()
  year: number;

  @Column({nullable: true})
  rated: string;

  @Column({nullable: true})
  released: string;

  @Column({nullable: true})
  runtime: string;

  @Column({nullable: true})
  genre: string;

  @Column({nullable: true})
  director: string;

  @Column({type: 'text', nullable: true})
  writer: string;

  @Column({type: 'text', nullable: true})
  actors: string;

  @Column({type: 'text', nullable: true})
  shortPlot: string;

  @Column({type: 'text', nullable: true})
  fullPlot: string;

  @Column({nullable: true})
  language: string;

  @Column({nullable: true})
  country: string;

  @Column({type: 'text', nullable: true})
  awards: string;

  @Column({type: 'text', nullable: true})
  poster: string;

  @OneToMany(type => MovieRatingEntity, movieRating => movieRating.movie, {cascade: true, eager: true})
  ratings: MovieRatingEntity[];

  @Column({nullable: true})
  metascore: string;

  @Column({nullable: true})
  imdbRating: string;

  @Column({nullable: true})
  imdbVotes: string;

  @Column({primary: true, unique: true})
  imdbID: string;

  @Column({type: 'varchar'})
  type: TOMDBMovieType;

  @Column({nullable: true})
  dvd: string;

  @Column({nullable: true})
  boxOffice: string;

  @Column({nullable: true})
  production: string;

  @Column({nullable: true})
  website: string;

  @Type(type => MovieCommentEntity)
  @OneToMany(type => MovieCommentEntity, movieComment => movieComment.movie)
  comments: MovieCommentEntity[];

  constructor(ctx: DeepPartial<MovieEntity>) {
    super();
    Object.assign(this, ctx);
  }

}
