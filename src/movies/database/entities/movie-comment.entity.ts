import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {MovieEntity} from './movie.entity';
import {BaseEntity} from '../../../database/base-entity';
import {Type} from 'class-transformer';

@Entity()
export class MovieCommentEntity extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Type(type => MovieEntity)
  @ManyToOne(type => MovieEntity, movie => movie.comments)
  movie: MovieEntity;

  @Column()
  ip: string;

  @Column({type: 'text'})
  text: string;

  constructor(ctx: Partial<MovieCommentEntity>) {
    super();
    Object.assign(this, ctx);
  }

}