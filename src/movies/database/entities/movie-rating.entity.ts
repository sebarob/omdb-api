import {Column, Entity, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import {MovieEntity} from './movie.entity';

@Entity()
export class MovieRatingEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => MovieEntity, movie => movie.ratings)
  movie: MovieEntity;

  @Column()
  source: string;

  @Column()
  value: string;

  constructor(ctx: Partial<MovieRatingEntity>) {
    Object.assign(this, ctx);
  }

}