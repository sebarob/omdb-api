import {HttpModule, Module, OnModuleInit} from '@nestjs/common';
import {MoviesController} from './controllers/movies.controller';
import {MovieService} from './services/movie/movie.service';
import {MovieLoaderService} from './services/movie/movies-loader/movie-loader.service';
import {MovieQueueService} from './services/movie/movie-queue.service';
import {MovieEntity} from './database/entities/movie.entity';
import {MovieRatingEntity} from './database/entities/movie-rating.entity';
import {MovieRepository} from './database/repositories/movies.repository';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ConfigurationModule} from '../configuration/configuration.module';
import {MovieSubscriber} from './database/subscribers/movie.subscriber';
import {SearchModule} from '../search/search.module';
import {ElasticsearchService} from '../search/elasticsearch.service';
import {CommentsController} from './controllers/comments.controller';
import {CommentService} from './services/comment/comment.service';
import {MovieCommentRepository} from './database/repositories/comment.repository';
import {ConfigProvider} from '../configuration/helpers';

export const movieIndex = 'movie';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MovieEntity,
      MovieRatingEntity,
      MovieRepository,
      MovieCommentRepository,
    ]),
    HttpModule,
    ConfigurationModule,
    SearchModule,
  ],
  controllers: [
    MoviesController,
    CommentsController,
  ],
  providers: [
    ConfigProvider,
    MovieService,
    MovieLoaderService,
    MovieQueueService,
    MovieSubscriber,
    CommentService,
  ],
})
export class MoviesModule implements OnModuleInit {
  constructor(private searchService: ElasticsearchService) {
  }

  onModuleInit() {
    this.searchService.registerIndice(movieIndex);
  }
}
