import {Injectable} from '@nestjs/common';
import {MovieRepository} from '../../database/repositories/movies.repository';
import {InjectRepository} from '@nestjs/typeorm';
import {MovieLoaderService} from './movies-loader/movie-loader.service';
import {map, switchMap} from 'rxjs/operators';
import {combineLatest, from} from 'rxjs';
import {ElasticsearchService} from '../../../search/elasticsearch.service';
import {MovieFetchRequestDto} from '../../dtos/movie';
import {movieIndex} from '../../movies.module';
import {IPaginatedData} from '../../../database/repository-helper';
import {MovieEntity} from '../../database/entities/movie.entity';

@Injectable()
export class MovieService {

  constructor(
      @InjectRepository(MovieRepository)
      private readonly movieRepository: MovieRepository,
      private readonly movieLoaderService: MovieLoaderService,
      private readonly searchService: ElasticsearchService,
  ) {
  }

  getMovie(id) {
    return this.movieRepository.findOne({id});
  }

  getMovies() {
    return this.movieRepository.find();
  }

  searchMovies(search) {
    if (!search) {
      return this.getMovies();
    }

    const search$ = from(this.searchService.search({
      index: movieIndex,
      type: movieIndex,
      body: {
        query: {
          multi_match: {
            query: search,
            fields: ['actors', 'title', 'shortPlot', 'fullPlot', 'writer'],
          },
        },
      },
    }));

    return search$.pipe(
        map(result => result.hits.hits),
        switchMap(hits => from(
            this.movieRepository.findByIds(hits.map(hit => hit._id)),
            ),
        ),
    );
  }

  getWithPagination(paginationParams): Promise<IPaginatedData<MovieEntity>> {
    return this.movieRepository.paginate(paginationParams);
  }

  getWithComments(id) {
    return this.movieRepository.findOne({
      where: {id},
      relations: ['comments'],
    });
  }

  fetchMovies(body: MovieFetchRequestDto) {
    if (body.id) {
      return this.movieLoaderService.fetchMoviesById(body).pipe(
          switchMap(movie => from(this.movieRepository.save(movie))),
      );
    } else if (body.title) {
      return this.movieLoaderService.fetchMoviesByTitle(body).pipe(
          switchMap(movie => from(this.movieRepository.save(movie))),
      );
    } else if (body.search && body.pages == null) {
      return this.movieLoaderService.fetchMoviesBySearch(body).pipe(
          switchMap(movies => from(this.movieRepository.save(movies))),
      );
    } else if (body.search && body.pages) { // Makes n + 1 requests to OMDB API
      return this.movieLoaderService.fetchMultiplePages(body).pipe(
          switchMap(createdJobs => combineLatest(createdJobs.map(async createdJob => {
            const result = await createdJob.finished();
            return {
              params: createdJob.data,
              rows: result,
            };
          }))),
      );
    }
  }

}