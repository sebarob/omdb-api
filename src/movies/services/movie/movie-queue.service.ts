import * as Queue from 'bull';
import {forwardRef, Inject, Injectable, OnModuleInit} from '@nestjs/common';
import {OMDBApiParamsBySearch} from './movies-loader/interfaces';
import {MovieRepository} from '../../database/repositories/movies.repository';
import {InjectRepository} from '@nestjs/typeorm';
import {MovieLoaderService} from './movies-loader/movie-loader.service';
import * as Bluebird from 'bluebird';
import {APP_CONFIG} from '../../../configuration/helpers';
import {AppConfig} from '../../../configuration/interfaces';

@Injectable()
export class MovieQueueService implements OnModuleInit {

  private movieQueue: Queue.Queue<OMDBApiParamsBySearch> = new Queue('movie-fetch', {redis: this.config.redis});

  constructor(
      @InjectRepository(MovieRepository)
      private movieRepository: MovieRepository,
      @Inject(forwardRef(() => MovieLoaderService))
      private movieLoaderService: MovieLoaderService,
      @Inject(APP_CONFIG)
      private readonly config: AppConfig,
  ) {
  }

  onModuleInit() {
    this.processFetchQueue();
  }

  addFetchJob(params: OMDBApiParamsBySearch) {
    return this.movieQueue.add(params);
  }

  processFetchQueue() {
    this.movieQueue.process(2, this.fetchJobHandler.bind(this));
  }

  async fetchJobHandler(job) {
    const movies = await this.movieLoaderService.fetchMoviesBySearch(job.data).toPromise();
    return this.movieRepository.save(movies);
  }
}

