import {Test} from '@nestjs/testing';
import {MovieRepository} from '../../database/repositories/movies.repository';
import {getRepositoryToken} from '@nestjs/typeorm';
import {MovieLoaderService} from './movies-loader/movie-loader.service';
import {of} from 'rxjs';
import {MovieService} from './movie.service';
import {ElasticsearchService} from '../../../search/elasticsearch.service';
import {MovieFetchRequestDto} from '../../dtos/movie';

describe('Movie service', () => {
  let app;
  let mockedReposiotry;
  let movieLoaderService: MovieLoaderService;
  let searchService: Partial<ElasticsearchService>;
  let movieService: MovieService;

  beforeAll(async () => {
    mockedReposiotry = {
      async save() {
      },
      async findOne() {
      },
      async find() {
      },
      async paginate() {
      },
    };
    movieLoaderService = {
      async fetchMoviesById() {
      },
      async fetchMoviesByTitle() {
      },
      async fetchMoviesBySearch() {
      },
      async fetchMultiplePages() {
      },
    } as any;
    searchService = {
      async search() {
      },
    } as any;

    app = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(MovieRepository),
          useValue: mockedReposiotry,
        },
        {
          provide: MovieLoaderService,
          useValue: movieLoaderService,
        },
        {
          provide: ElasticsearchService,
          useValue: searchService,
        },
        MovieService,
      ],
    }).compile();

    movieService = app.get(MovieService);
  });

  it('should get movie by ID', async () => {
    const spy = jest.spyOn(mockedReposiotry, 'findOne');

    await movieService.getMovie(1);
    expect(spy).toHaveBeenCalledWith({id: 1});
  });

  it('should get all movies', async () => {
    const spy = jest.spyOn(mockedReposiotry, 'find');

    await movieService.getMovies();
    expect(spy).toHaveBeenCalledWith();
  });

  it('should get movies with pagination', async () => {
    const spy = jest.spyOn(mockedReposiotry, 'paginate');
    const paginationParams = {pageIndex: 1, pageSize: 15};

    await movieService.getWithPagination(paginationParams);
    expect(spy).toHaveBeenCalledWith(paginationParams);
  });

  it('should get movies with comments', async () => {
    const spy = jest.spyOn(mockedReposiotry, 'findOne');

    await movieService.getWithComments(1);
    expect(spy).toHaveBeenCalledWith({
      where: {id: 1},
      relations: ['comments'],
    });
  });

  describe('fetch to db', () => {
    let spyFetchByID;
    let spyFetchByTitle;
    let spyFetchBySearch;
    let spyFetchPages;
    let spyRepository;

    beforeEach(async () => {
      jest.clearAllMocks();
      spyFetchByID = jest.spyOn(movieLoaderService, 'fetchMoviesById').mockImplementation((v) => of(v));
      spyFetchByTitle = jest.spyOn(movieLoaderService, 'fetchMoviesByTitle').mockImplementation((v) => of(v));
      spyFetchBySearch = jest.spyOn(movieLoaderService, 'fetchMoviesBySearch').mockImplementation((v) => of(v));
      spyFetchPages = jest.spyOn(movieLoaderService, 'fetchMultiplePages').mockImplementation((v) => of(
          Array(10).fill(0).map((_, i) => ({
            data: i,
            finished: async () => v,
          })),
      ));
      spyRepository = jest.spyOn(mockedReposiotry, 'save').mockImplementation(async (v) => (v));
    });

    it('should fetch movie based on id', async () => {
      const query = {id: '1'};
      const result = await movieService.fetchMovies(query as MovieFetchRequestDto).toPromise();

      expect(result).toBe(query);
      expect(spyRepository).toHaveBeenCalledWith(query);

      expect(spyFetchByTitle).not.toHaveBeenCalled();
      expect(spyFetchBySearch).not.toHaveBeenCalled();
      expect(spyFetchPages).not.toHaveBeenCalled();
    });

    it('should fetch movie based on title', async () => {
      const query = {title: '1'};
      const result = await movieService.fetchMovies(query as MovieFetchRequestDto).toPromise();

      expect(result).toBe(query);
      expect(spyRepository).toHaveBeenCalledWith(query);

      expect(spyFetchByID).not.toHaveBeenCalled();
      expect(spyFetchBySearch).not.toHaveBeenCalled();
      expect(spyFetchPages).not.toHaveBeenCalled();
    });

    it('should fetch movie based on search and page', async () => {
      const query = {search: '1', page: 1};
      const result = await movieService.fetchMovies(query as MovieFetchRequestDto).toPromise();

      expect(result).toBe(query);
      expect(spyRepository).toHaveBeenCalledWith(query);

      expect(spyFetchByID).not.toHaveBeenCalled();
      expect(spyFetchByTitle).not.toHaveBeenCalled();
      expect(spyFetchPages).not.toHaveBeenCalled();
    });

    it('should add fetch jobs to queue', async () => {
      const query = {search: '1', pages: 10};
      const result = await movieService.fetchMovies(query as MovieFetchRequestDto).toPromise();

      expect(result).toEqual(
          Array(10).fill(0).map((_, i) => ({
            params: i,
            rows: query,
          })),
      );
      expect(spyRepository).not.toHaveBeenCalled();

      expect(spyFetchByID).not.toHaveBeenCalled();
      expect(spyFetchByTitle).not.toHaveBeenCalled();
      expect(spyFetchBySearch).not.toHaveBeenCalled();
    });
  });

});
