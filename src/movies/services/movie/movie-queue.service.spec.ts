import {Test} from '@nestjs/testing';
import {MovieRepository} from '../../database/repositories/movies.repository';
import {getRepositoryToken} from '@nestjs/typeorm';
import {MovieQueueService} from './movie-queue.service';
import {MovieLoaderService} from './movies-loader/movie-loader.service';
import {of} from 'rxjs';
import {ConfigProvider} from '../../../configuration/helpers';
import {ConfigurationModule} from '../../../configuration/configuration.module';

describe('Movie queue service', () => {
  let app;
  let mockedReposiotry;
  let movieQueueService: MovieQueueService;
  let movieLoaderService: MovieLoaderService;
  let movieQueue: any;

  beforeAll(async () => {
    mockedReposiotry = {
      async save() {
      },
      async findOne() {
      },
      async find() {
      },
    };
    movieLoaderService = {
      fetchMoviesBySearch: () => {
      },
    } as any;

    app = await Test.createTestingModule({
      imports: [ConfigurationModule],
      providers: [
        {
          provide: getRepositoryToken(MovieRepository),
          useValue: mockedReposiotry,
        },
        {
          provide: MovieLoaderService,
          useValue: movieLoaderService,
        },
        MovieQueueService,
        ConfigProvider,
      ],
    }).compile();

    movieQueueService = app.get(MovieQueueService);
    movieQueue = {
      async add() {
      },
      async process() {
      },
    };
    (movieQueueService as any).movieQueue = movieQueue;
  });

  it('should start processing queue on init', () => {
    const spy = jest.spyOn(movieQueue, 'process' as any);

    movieQueueService.onModuleInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should fetch movies and save to db', async () => {
    const spy = jest.spyOn(mockedReposiotry, 'save');
    jest.spyOn(movieLoaderService, 'fetchMoviesBySearch').mockImplementation(() => of([{}]));

    await movieQueueService.fetchJobHandler({data: {test: 1}});

    expect(spy).toHaveBeenCalledWith([{}]);
  });

  it('should add movie to queue', () => {
    const spy = jest.spyOn(movieQueue, 'add' as any);

    movieQueueService.addFetchJob({} as any);

    expect(spy).toHaveBeenCalledWith({});
  });

});