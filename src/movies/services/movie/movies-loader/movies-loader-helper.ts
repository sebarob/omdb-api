import {OMDBApiParamsByID, OMDBApiParamsBySearch, OMDBApiParamsByTitle, OMDBMoive, PlotTypes, TPlot} from './interfaces';
import {MovieEntity} from '../../../database/entities/movie.entity';
import {MovieRatingEntity} from '../../../database/entities/movie-rating.entity';
import {DeepPartial} from 'typeorm';

export const defaultFetchParamsByID: Partial<OMDBApiParamsByID> = {
  plot: PlotTypes.Short,
};

export const defaultFetchParamsByTitle: Partial<OMDBApiParamsByTitle> = defaultFetchParamsByID;

export const defaultFetchParamsBySearch: Partial<OMDBApiParamsBySearch> = {
  page: 1,
};

export const setDefaults = (object, defaults) => {
  return Object.entries(defaults).reduce((acc, [key, value]) => {
    if (object[key] == null) {
      return {...acc, [key]: value};
    } else {
      return object;
    }
  }, object);
};

export const readableToOMDBParams = (params) => {
  const OMDBParamsMap = {
    id: 'i',
    title: 't',
    search: 's',
    type: 'type',
    year: 'y',
    plot: 'plot',
    r: 'response',
    page: 'page',
  };

  return Object.entries(params).reduce((acc, [key, value]) => ({...acc, [OMDBParamsMap[key]]: value}), {});
};

export interface ParserOptions {
  plot: TPlot;
}

export const omdbMovieParser = (movie: DeepPartial<OMDBMoive>, options: ParserOptions): MovieEntity => {
  let plot = {};
  if (options.plot === PlotTypes.Full) {
    plot = {fullPlot: movie.Plot};
  } else if (options.plot === PlotTypes.Short) {
    plot = {shortPlot: movie.Plot};
  }

  if (!movie.Ratings) {
    movie.Ratings = [];
  }

  return new MovieEntity({
    ...plot,
    title: movie.Title,
    year: parseInt(movie.Year, 10),
    rated: movie.Rated,
    released: movie.Released,
    runtime: movie.Runtime,
    genre: movie.Genre,
    director: movie.Director,
    writer: movie.Writer,
    actors: movie.Actors,
    language: movie.Language,
    country: movie.Country,
    awards: movie.Awards,
    poster: movie.Poster !== 'N/A' ? movie.Poster : null,
    ratings: movie.Ratings.map(rating => new MovieRatingEntity({
      source: rating.Source,
      value: rating.Value,
    })),
    metascore: movie.Metascore,
    imdbRating: movie.imdbRating,
    imdbVotes: movie.imdbVotes,
    imdbID: movie.imdbID,
    type: movie.Type,
    dvd: movie.DVD,
    boxOffice: movie.BoxOffice,
    production: movie.Production,
    website: movie.Website,
  });
}
