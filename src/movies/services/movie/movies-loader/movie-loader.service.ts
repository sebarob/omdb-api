import {forwardRef, HttpException, HttpService, HttpStatus, Inject, Injectable} from '@nestjs/common';
import {MovieRepository} from '../../../database/repositories/movies.repository';
import {InjectRepository} from '@nestjs/typeorm';
import {OMDBApiParamsByID, OMDBApiParamsBySearch, OMDBApiParamsByTitle, OMDBMoive, OMDBSearchResult, PlotTypes} from './interfaces';
import {
  defaultFetchParamsByID,
  defaultFetchParamsBySearch,
  defaultFetchParamsByTitle,
  omdbMovieParser,
  readableToOMDBParams,
  setDefaults,
} from './movies-loader-helper';
import {map, switchMap} from 'rxjs/operators';
import * as Queue from 'bull';
import {MovieQueueService} from '../movie-queue.service';
import {from} from 'rxjs';
import Bluebird = require('bluebird');
import {AppConfig} from '../../../../configuration/interfaces';
import {APP_CONFIG} from '../../../../configuration/helpers';

@Injectable()
export class MovieLoaderService {

  constructor(
      @InjectRepository(MovieRepository)
      private readonly movieRepository: MovieRepository,
      @Inject(APP_CONFIG)
      private readonly config: AppConfig,
      @Inject(forwardRef(() => MovieQueueService))
      private readonly movieQueueService: MovieQueueService,
      private readonly http: HttpService,
  ) {
  }

  public fetchMoviesById(params: OMDBApiParamsByID) {
    const paramsWithDefaults = setDefaults(params, defaultFetchParamsByID);
    return this.fetchMovies<OMDBMoive>(paramsWithDefaults).pipe(
        map(movie => omdbMovieParser(movie, {plot: paramsWithDefaults.plot})),
    );
  }

  public fetchMoviesByTitle(params: OMDBApiParamsByTitle) {
    const paramsWithDefaults = setDefaults(params, defaultFetchParamsByTitle);
    return this.fetchMovies<OMDBMoive>(paramsWithDefaults).pipe(
        map(movie => omdbMovieParser(movie, {plot: paramsWithDefaults.plot})),
    );
  }

  public fetchMoviesBySearch(params: OMDBApiParamsBySearch) {
    const paramsWithDefaults = setDefaults(params, defaultFetchParamsBySearch);
    return this.fetchMovies<OMDBSearchResult>(paramsWithDefaults).pipe(
        map(movieSearch => movieSearch.Search.map(movie => omdbMovieParser(movie, {plot: PlotTypes.Short}))),
    );
  }

  private fetchAvilablePages(params: OMDBApiParamsBySearch) {
    return this.fetchMovies<OMDBSearchResult>(params).pipe(
        map(movieSearch => {
          const total = parseInt(movieSearch.totalResults, 10);

          return {
            rows: movieSearch.Search.map(movie => omdbMovieParser(movie, {plot: PlotTypes.Short})),
            totalResults: total,
            pages: Math.ceil(total / 10),
          };
        }),
    );
  }

  public fetchMultiplePages(params) {
    const {pages, ...job} = params;

    return this.fetchAvilablePages(params).pipe(
        switchMap(avilablePages => {
          let pagesToFetch = pages;
          if (avilablePages.pages < pages) {
            pagesToFetch = avilablePages.pages;
          }

          let loops = pagesToFetch;
          const addedJobs: Bluebird<Queue.Job<OMDBApiParamsBySearch>>[] = [];
          while (loops-- > 0) {
            const addedJob = this.movieQueueService.addFetchJob({
              ...job,
              page: pagesToFetch - loops,
            });
            addedJobs.push(addedJob);
          }

          return from(Promise.all(addedJobs));
        }),
    );
  }

  private fetchMovies<T>(params) {
    return this.http.get<T>(this.config.movieApi.url, {
      params: {
        ...readableToOMDBParams(params),
        apiKey: this.config.movieApi.key,
      },
    }).pipe(
        map(response => response.data),
        map((data: any) => {
          if (data.Response === 'False' || data.Error != null) {
            throw new HttpException(data.Error || 'OMDB API Error', HttpStatus.SERVICE_UNAVAILABLE);
          }
          return data;
        }),
    );
  }

}