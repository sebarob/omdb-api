export enum PlotTypes {
  Short = 'short',
  Full = 'full',
}

export enum MovieTypes {
  Movie = 'movie',
  Series = 'series',
  Episode = 'episode',
}

export type TOMDBMovieType = MovieTypes.Movie | MovieTypes.Series | MovieTypes.Episode;
export type TPlot = PlotTypes.Short | PlotTypes.Full;

interface OMDBApiBase {
  type: TOMDBMovieType;
  year: number;
}

export interface OMDBApiParamsByID extends OMDBApiBase {
  id: string;
  plot: TPlot;
}

export interface OMDBApiParamsByTitle extends OMDBApiBase {
  title: string;
  plot: TPlot;
}

export interface OMDBApiParamsBySearch extends OMDBApiBase {
  search: string;
  page: number;
}

export interface OMDBMovieRating {
  Source: string;
  Value: string;
}

export interface OMDBMoive {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: OMDBMovieRating[];
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: TOMDBMovieType;
  DVD: string;
  BoxOffice: string;
  Production: string;
  Website: string;
  Response: string;
}

export interface OMDBSearchResult {
  Search: Partial<OMDBMoive>[];
  totalResults: string;
  Response: string;
}