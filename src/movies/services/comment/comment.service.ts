import {Injectable} from '@nestjs/common';
import {MovieCommentRepository} from '../../database/repositories/comment.repository';
import {InjectRepository} from '@nestjs/typeorm';

@Injectable()
export class CommentService {

  constructor(
      @InjectRepository(MovieCommentRepository)
      private readonly commentRepository: MovieCommentRepository,
  ) {
  }

  createComment(ip, {text, imdbID}) {
    return this.commentRepository.save({
      ip,
      text,
      movie: imdbID,
    });
  }

  getComment(id) {
    return this.commentRepository.findOne({id});
  }

  getAllComments() {
    return this.commentRepository.find();
  }

  getCommentWithMovie(id) {
    return this.commentRepository.findOne({
      relations: ['movie'],
      where: {id},
    });
  }
}