import {Test} from '@nestjs/testing';
import {MovieCommentRepository} from '../../database/repositories/comment.repository';
import {getRepositoryToken} from '@nestjs/typeorm';
import {CommentService} from './comment.service';

describe('Comment service', () => {
  let app;
  let commentService: CommentService;
  let mockedReposiotry;

  beforeAll(async () => {
    mockedReposiotry = {
      async save() {},
      async findOne() {},
      async find() {},
    };

    app = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(MovieCommentRepository),
          useValue: mockedReposiotry,
        },
        CommentService,
      ],
    }).compile();

    commentService = app.get(CommentService);
  });

  it('should save comment in database', async () => {
    const save = jest.spyOn(mockedReposiotry, 'save');

    await commentService.createComment('ip', {text: 'text', imdbID: 'imdbID'});

    expect(save).toHaveBeenCalledWith({
      ip: 'ip',
      text: 'text',
      movie: 'imdbID',
    });
  });

  it('should get comment by id', async () => {
    const findOne = jest.spyOn(mockedReposiotry, 'findOne');

    await commentService.getComment(1);

    expect(findOne).toHaveBeenCalledWith({id: 1});
  });

  it('should get all comments', async () => {
    const find = jest.spyOn(mockedReposiotry, 'find');

    await commentService.getAllComments();

    expect(find).toHaveBeenCalledWith();
  });

  it('should get comment with movie', async () => {
    const findOne = jest.spyOn(mockedReposiotry, 'findOne');

    await commentService.getCommentWithMovie(1);

    expect(findOne).toHaveBeenCalledWith({
      relations: ['movie'],
      where: {id: 1},
    });
  });

});
