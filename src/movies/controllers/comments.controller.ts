import {Body, ClassSerializerInterceptor, Controller, Get, Param, ParseIntPipe, Post, UseInterceptors, ValidationPipe} from '@nestjs/common';
import {IPAddress} from '../../decorators/ip-address';
import {CommentService} from '../services/comment/comment.service';
import {CommentCreateReqDto, CommentResDto, CommentWithMovieResDto} from '../dtos/comment';
import {ApiResponse, ApiUseTags} from '@nestjs/swagger';

@Controller('/comments')
@ApiUseTags('comments')
@UseInterceptors(ClassSerializerInterceptor)
export class CommentsController {

  @Get()
  @ApiResponse({status: 200, description: 'Returns list of all comments', type: CommentResDto, isArray: true})
  getComments() {
    return this.commentService.getAllComments();
  }

  @Post()
  @ApiResponse({status: 200, description: 'Creates new comment and assign it to movie'})
  createComment(
      @Body(new ValidationPipe()) comment: CommentCreateReqDto,
      @IPAddress() ip,
  ) {
    return this.commentService.createComment(ip, comment);
  }

  @Get('/:id/movie')
  @ApiResponse({status: 200, description: 'Returns comment with movie', type: CommentWithMovieResDto})
  getCommentWithMovie(
      @Param('id', ParseIntPipe) id,
  ) {
    return this.commentService.getCommentWithMovie(id);
  }

  @Get('/:id')
  @ApiResponse({status: 200, description: 'Returns comment by ID', type: CommentResDto})
  getComment(
      @Param('id', ParseIntPipe) id,
  ) {
    return this.commentService.getComment(id);
  }

  constructor(private commentService: CommentService) {
  }

}