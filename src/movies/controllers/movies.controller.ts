import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  Req,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import {MovieService} from '../services/movie/movie.service';
import {ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {PaginatedReqQueryDto} from '../../dtos/paginated-req-query.dto';
import {MovieFetchRequestDto, MoviePaginatedResDto, MovieResDto, MovieWithCommentsResDto} from '../dtos/movie';
import {PaginationParams} from '../../decorators';
import {IPaginatedData} from '../../database/repository-helper';

@Controller('/movies')
@ApiUseTags('movies')
export class MoviesController {

  @UseInterceptors(ClassSerializerInterceptor)
  @ApiResponse({status: 200, description: 'Returns list of all movies', isArray: true, type: MovieResDto})
  @Get()
  getAllMovies() {
    return this.movieService.getMovies();
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @ApiResponse({status: 200, description: 'Returns list of all movies', isArray: true, type: MovieResDto})
  @Get('/search')
  searchMovies(
      @Query('search') search,
  ) {
    return this.movieService.searchMovies(search);
  }

  @Post()
  @ApiResponse({status: 201, description: 'Fetches movies from OMDB Api to server database'})
  fetchMovies(
      @Body(new ValidationPipe()) body: MovieFetchRequestDto,
      @Req() req,
  ) {
    return this.movieService.fetchMovies(body);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @ApiResponse({status: 200, description: 'Returns paginated list of movies', type: MoviePaginatedResDto})
  @Get('/paginated')
  getPaginated(
      @PaginationParams(new ValidationPipe()) paginationParams: PaginatedReqQueryDto,
  ) {
    return this.movieService.getWithPagination(paginationParams);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @ApiResponse({status: 200, description: 'Returns movie by ID', type: MovieResDto})
  @Get('/:id')
  getMovie(
      @Param('id') id,
  ) {
    return this.movieService.getMovie(id);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @ApiResponse({status: 200, description: 'Returns movie by ID with comments', type: MovieWithCommentsResDto})
  @Get('/:id/comments')
  getMovieWithComments(
      @Param('id') id,
  ) {
    return this.movieService.getWithComments(id);
  }

  constructor(private movieService: MovieService) {
  }
}