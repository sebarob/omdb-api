import {ApiModelProperty} from '@nestjs/swagger';
import {PaginatedResDto} from '../../../dtos/paginated-res.dto';
import {MovieResDto} from './movie-res.dto';

export class MoviePaginatedResDto extends PaginatedResDto {

  @ApiModelProperty({type: MovieResDto, isArray: true})
  rows: MovieResDto[];

}