import {
  MovieTypes,
  OMDBApiParamsByID,
  OMDBApiParamsBySearch,
  OMDBApiParamsByTitle,
  PlotTypes,
  TOMDBMovieType,
  TPlot,
} from '../../services/movie/movies-loader/interfaces';
import {IsIn, IsNumber, IsOptional, IsString, Max, Min} from 'class-validator';
import {ThrowIf} from '../../../validators/throw-if';
import {ApiModelPropertyOptional} from '@nestjs/swagger';

const errorMessage = {
  notCombineMainFilters: 'You cannot combine title, id, search params',
  notCombinePage: 'You cannot combine page and pages params',
};

export class MovieFetchRequestDto implements OMDBApiParamsByID, OMDBApiParamsByTitle, OMDBApiParamsBySearch {

  @ApiModelPropertyOptional({description: 'You must provide one of title, id, search params'})
  @ThrowIf((self: MovieFetchRequestDto) => self.title || self.search, {message: errorMessage.notCombineMainFilters})
  @IsOptional()
  @IsString()
  id: string;

  @ApiModelPropertyOptional({description: 'You must provide one of title, id, search params'})
  @ThrowIf((self: MovieFetchRequestDto) => self.id || self.search, {message: errorMessage.notCombineMainFilters})
  @IsOptional()
  @IsString()
  title: string;

  @ApiModelPropertyOptional({description: 'You must provide one of title, id, search params'})
  @ThrowIf((self: MovieFetchRequestDto) => self.id || self.title, {message: errorMessage.notCombineMainFilters})
  @IsOptional()
  @IsString()
  search: string;

  @ApiModelPropertyOptional({description: 'Filter results by type', enum: [MovieTypes.Movie, MovieTypes.Series, MovieTypes.Episode]})
  @IsOptional()
  @IsString()
  @IsIn([MovieTypes.Movie, MovieTypes.Series, MovieTypes.Episode])
  type: TOMDBMovieType;

  @ApiModelPropertyOptional({description: 'Available in id or title mode', enum: [PlotTypes.Short, PlotTypes.Full]})
  @IsOptional()
  @IsString()
  @IsIn([PlotTypes.Short, PlotTypes.Full])
  plot: TPlot;

  @ApiModelPropertyOptional({description: 'Filter results by year'})
  @IsOptional()
  @IsNumber()
  year: number;

  @ApiModelPropertyOptional({description: 'Avilable in search mode, cannot be used with pages param. Fetches selected page'})
  @IsOptional()
  @ThrowIf((self: MovieFetchRequestDto) => self.pages != null, {message: errorMessage.notCombinePage})
  @IsNumber()
  @Min(1)
  @Max(100)
  page: number;

  @ApiModelPropertyOptional({description: 'Avilable in search mode, cannot be used with page param. Fetches selected number of pages'})
  @IsOptional()
  @ThrowIf((self: MovieFetchRequestDto) => self.page != null, {message: errorMessage.notCombinePage})
  @IsNumber()
  @Min(1)
  @Max(100)
  pages: number;
}