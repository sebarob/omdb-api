import {MovieRatingEntity} from '../../database/entities/movie-rating.entity';
import {MovieTypes, TOMDBMovieType} from '../../services/movie/movies-loader/interfaces';
import {ApiModelProperty, ApiModelPropertyOptional} from '@nestjs/swagger';
import {MovieRatingResDto} from './movie-rating-res.dto';

export class MovieResDto {

  @ApiModelProperty()
  id: string;

  @ApiModelProperty()
  title: string;

  @ApiModelProperty()
  year: number;

  @ApiModelPropertyOptional()
  rated: string;

  @ApiModelPropertyOptional()
  released: string;

  @ApiModelPropertyOptional()
  runtime: string;

  @ApiModelPropertyOptional()
  genre: string;

  @ApiModelPropertyOptional()
  director: string;

  @ApiModelPropertyOptional()
  writer: string;

  @ApiModelPropertyOptional()
  actors: string;

  @ApiModelPropertyOptional()
  shortPlot: string;

  @ApiModelPropertyOptional()
  fullPlot: string;

  @ApiModelPropertyOptional()
  language: string;

  @ApiModelPropertyOptional()
  country: string;

  @ApiModelPropertyOptional()
  awards: string;

  @ApiModelProperty()
  poster: string;

  @ApiModelPropertyOptional({isArray: true, type: MovieRatingResDto})
  ratings: MovieRatingEntity[];

  @ApiModelPropertyOptional()
  metascore: string;

  @ApiModelPropertyOptional()
  imdbRating: string;

  @ApiModelPropertyOptional()
  imdbVotes: string;

  @ApiModelProperty()
  imdbID: string;

  @ApiModelProperty({type: String, enum: [MovieTypes.Episode, MovieTypes.Series, MovieTypes.Movie]})
  type: TOMDBMovieType;

  @ApiModelPropertyOptional()
  dvd: string;

  @ApiModelPropertyOptional()
  boxOffice: string;

  @ApiModelPropertyOptional()
  production: string;

  @ApiModelPropertyOptional()
  website: string;

}