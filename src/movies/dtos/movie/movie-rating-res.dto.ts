import {ApiModelProperty} from '@nestjs/swagger';

export class MovieRatingResDto {

  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  source: string;

  @ApiModelProperty()
  value: string;

}