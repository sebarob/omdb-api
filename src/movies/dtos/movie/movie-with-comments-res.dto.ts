import {MovieResDto} from './movie-res.dto';
import {ApiModelProperty} from '@nestjs/swagger';
import {CommentResDto} from '../comment/comment-res.dto';

export class MovieWithCommentsResDto extends MovieResDto {

  @ApiModelProperty({type: CommentResDto, isArray: true})
  comments: CommentResDto[];

}