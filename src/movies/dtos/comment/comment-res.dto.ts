import {ApiModelProperty} from '@nestjs/swagger';

export class CommentResDto {

  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  ip: string;

  @ApiModelProperty()
  text: string;

}