import {MovieEntity} from '../../database/entities/movie.entity';
import {IsString, MaxLength} from 'class-validator';
import {IsInDatabase} from '../../../validators/is-in-database';
import {ApiModelProperty} from '@nestjs/swagger';

export class CommentCreateReqDto {

  @ApiModelProperty()
  @IsInDatabase(MovieEntity, 'imdbID', {message: `Movie doesn't exist`})
  imdbID: string;

  @ApiModelProperty()
  @IsString()
  @MaxLength(1024)
  text: string;

}