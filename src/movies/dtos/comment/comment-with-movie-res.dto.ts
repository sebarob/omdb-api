import {ApiModelProperty} from '@nestjs/swagger';
import {MovieResDto} from '../movie/movie-res.dto';
import {CommentResDto} from './comment-res.dto';

export class CommentWithMovieResDto extends CommentResDto {

  @ApiModelProperty({type: MovieResDto})
  movie: number;

}