FROM mhart/alpine-node:10 as build
WORKDIR /src

RUN apk add --no-cache git python make gcc g++

COPY ./package* /src/
RUN npm install
COPY . /src/
RUN npm run build:prod

ENTRYPOINT ["npm", "run", "start:prod"]